package com.nespresso.exercise.terrific_tree;


import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TerrificTree {

    private Branch trunk = new Branch();
    private static final float REBRANCH_LENGTH = 10;
    private static final float BRANCH_GROW_LENGTH_POWERED_BY_WATER = 0.5f;
    private static final float BRANCH_GROW_LENGTH_POWERED_BY_SUN = 1f;
    private static final float TRUNK_GROW_LENGTH = 1f;

    private void growBranches(Branch trunk) {
        for (Branch branch : trunk.getBranches()) {
            branch.grow((BRANCH_GROW_LENGTH_POWERED_BY_WATER ));
            growBranches(branch);
        }
    }

    private void waterTree(Branch trunk) {
        trunk.setWatered(true);
        for (Branch branch : trunk.getBranches()) {
            branch.setWatered(true);
            waterTree(branch);
        }
    }

    private void sunnifyTree(Branch trunk) {
        for (Branch branch : trunk.getBranches()) {
            if (branch.isLeaf()) {
                branch.grow(BRANCH_GROW_LENGTH_POWERED_BY_SUN);
            }
            if (branch.isWatered()) {
                branch.setLeaf(true);
            }
            sunnifyTree(branch);
        }
    }

    private boolean fertilizeTree(Branch trunk) {
        boolean rebranched = false;
        for (Branch branch : trunk.getBranches()) {
            if (branch.getBranches().size() > 0) {
                rebranched = fertilizeTree(branch);
            }
            else if (branch.getLength() >= REBRANCH_LENGTH) {
                Branch newBranch = new Branch();
                if (branch.isLeaf()) {
                    branch.setLeaf(false);
                    newBranch.setLeaf(true);
                }
                branch.addBranch(newBranch);
                rebranched = true;
            }
        }
        return rebranched;
    }

    public TerrificTree receive(String something) {
        Pattern p = Pattern.compile("[A-Z]+|\\d+");
        Matcher m = p.matcher(something);
        ArrayList<String> allMatches = new ArrayList<String>();
        while (m.find()) {
            allMatches.add(m.group());
        }

        int amount = allMatches.size() == 2 ? Integer.parseInt(allMatches.get(0)) : 1;
        Resource resource = Resource.fromValue(allMatches.size() == 2 ? allMatches.get(1) : allMatches.get(0));

        switch (resource) {
            case WATER: {
                trunk.grow(amount * TRUNK_GROW_LENGTH);
                for (int i = 0; i < amount; i++) {
                    growBranches(trunk);
                }
                waterTree(trunk);
            }
            break;
            case SUN: {
                if (trunk.getBranches().size() > 0) {
                    for (int i = 0; i < amount; i++) {
                        sunnifyTree(trunk);
                    }
                } else if (trunk.isWatered()) {
                    trunk.setLeaf(true);
                }
            }
            break;
            case FERTILIZER: {
                boolean rebranched = fertilizeTree(trunk);
                if (!rebranched && trunk.getLength() >= REBRANCH_LENGTH) {
                    Branch branch = new Branch();
                    if (trunk.isLeaf()) {
                        trunk.setLeaf(false);
                        branch.setLeaf(true);
                    }
                    trunk.addBranch(branch);
                }
            }
            break;
        }

        return this;
    }


    private String printBranch(Branch branch) {
        StringBuffer terrificTreeJson = new StringBuffer();
        terrificTreeJson.append("{");

        terrificTreeJson.append("\"length\":");
        if (((int) (branch.getLength() * 10) / 10) == branch.getLength()) {
            terrificTreeJson.append(String.format("%.0f", branch.getLength()));
        } else {
            terrificTreeJson.append(String.format("%.1f", branch.getLength()));
        }

        terrificTreeJson.append(", ");
        terrificTreeJson.append("\"leaf\":");
        terrificTreeJson.append(branch.isLeaf());

        terrificTreeJson.append(", ");
        terrificTreeJson.append("\"branches\":");
        terrificTreeJson.append("[");
        String separator = "";

        for (Branch childBranch : branch.getBranches()) {
            terrificTreeJson.append(separator);
            terrificTreeJson.append(printBranch(childBranch));
            separator = ",";
        }
        terrificTreeJson.append("]");

        terrificTreeJson.append("}");

        return terrificTreeJson.toString();
    }

    public String print() {
        return printBranch(trunk);
    }

}
