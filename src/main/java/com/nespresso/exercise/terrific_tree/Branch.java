package com.nespresso.exercise.terrific_tree;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by ac on 22/12/16.
 */
public class Branch {
    private float length = 5;
    private boolean leaf;
    private List<Branch> branches = new LinkedList<Branch>();
    private boolean watered = false;

    public Branch() {
    }

    public Branch(float length, boolean leaf, List<Branch> branches) {
        this.length = length;
        this.leaf = leaf;
        this.branches = branches;
    }

    public float getLength() {
        return length;
    }

    public void setLength(float length) {
        this.length = length;
    }

    public boolean isLeaf() {
        return leaf;
    }

    public void setLeaf(boolean leaf) {
        this.leaf = leaf;
    }

    public List<Branch> getBranches() {
        return branches;
    }

    public void setBranches(List<Branch> branches) {
        this.branches = branches;
    }

    public boolean isWatered() {
        return watered;
    }

    public void setWatered(boolean watered) {
        this.watered = watered;
    }

    public void addBranch(Branch branch) {
        this.branches.add(branch);
    }

    public void grow(float amount) {
        length += amount;
    }
}
