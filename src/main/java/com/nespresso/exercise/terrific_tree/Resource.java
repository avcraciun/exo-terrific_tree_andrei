package com.nespresso.exercise.terrific_tree;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ac on 22/12/16.
 */
public enum Resource {

    WATER("WATER"),
    SUN("SUN"),
    FERTILIZER("FERTILIZER");

    private String name;

    private static Map<String, Resource> namesMap = new HashMap<String, Resource>();

    static {
        namesMap.put("WATER", WATER);
        namesMap.put("SUN", SUN);
        namesMap.put("FERTILIZER", FERTILIZER);
    }

    Resource(String name) {
        this.name = name;
    }

    public static Resource fromValue(String value) {
        if (value != null) {
            return namesMap.get(value);
        }
        return null;
    }
}
