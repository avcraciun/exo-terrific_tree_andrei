package com.nespresso.exercise.terrific_tree;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class TerrificTreeTest {

    private TerrificTree tree;

    @Before
    public void plantTree() {
        tree = new TerrificTree();
    }

    @Test
    public void startsWithA5CmTrunk() {
        assertThat(tree.print(), is("{\"length\":5, \"leaf\":false, \"branches\":[]}"));
    }

    @Test
    public void extendsBaseBranchOneCmPerWater() {
        tree.receive("WATER");
        assertThat(tree.print(), is("{\"length\":6, \"leaf\":false, \"branches\":[]}"));
        tree.receive("2WATER");
        assertThat(tree.print(), is("{\"length\":8, \"leaf\":false, \"branches\":[]}"));
    }

    @Test
    public void growsLeafAfterOneHourOfSunAndSomeWater() {
        tree.receive("SUN");
        assertThat(tree.print(), is("{\"length\":5, \"leaf\":false, \"branches\":[]}"));
        tree.receive("WATER");
        tree.receive("SUN");
        assertThat(tree.print(), is("{\"length\":6, \"leaf\":true, \"branches\":[]}"));
    }

    @Test
    public void growsBranchAfterFertilizerIfTrunkIsAtLeast10Long() {
        tree.receive("2WATER").receive("FERTILIZER");
        assertThat(tree.print(), is("{\"length\":7, \"leaf\":false, \"branches\":[]}"));
        tree.receive("3WATER").receive("FERTILIZER");
        assertThat(tree.print(), is("{\"length\":10, \"leaf\":false, \"branches\":[{\"length\":5, \"leaf\":false, \"branches\":[]}]}"));
        tree.receive("FERTILIZER");
        assertThat(tree.print(), is("{\"length\":10, \"leaf\":false, \"branches\":[{\"length\":5, \"leaf\":false, \"branches\":[]},{\"length\":5, \"leaf\":false, \"branches\":[]}]}"));
    }

    @Test
    public void extendsBaseBranchOneCmPerWaterAndHalfAsMuchEachBranch() {
        tree.receive("5WATER").receive("FERTILIZER");
        assertThat(tree.print(), is("{\"length\":10, \"leaf\":false, \"branches\":[{\"length\":5, \"leaf\":false, \"branches\":[]}]}"));
        tree.receive("WATER");
        assertThat(tree.print(), is("{\"length\":11, \"leaf\":false, \"branches\":[{\"length\":5.5, \"leaf\":false, \"branches\":[]}]}"));
        tree.receive("FERTILIZER");
        assertThat(tree.print(), is("{\"length\":11, \"leaf\":false, \"branches\":[{\"length\":5.5, \"leaf\":false, \"branches\":[]},{\"length\":5, \"leaf\":false, \"branches\":[]}]}"));
        tree.receive("2WATER");
        assertThat(tree.print(), is("{\"length\":13, \"leaf\":false, \"branches\":[{\"length\":6.5, \"leaf\":false, \"branches\":[]},{\"length\":6, \"leaf\":false, \"branches\":[]}]}"));
    }

    @Test
    public void growsLeavesOnlyOnTerminalBranches() {
        tree.receive("5WATER").receive("FERTILIZER").receive("FERTILIZER");
        assertThat(tree.print(), is("{\"length\":10, \"leaf\":false, \"branches\":[{\"length\":5, \"leaf\":false, \"branches\":[]},{\"length\":5, \"leaf\":false, \"branches\":[]}]}"));
        tree.receive("SUN");
        assertThat(tree.print(), is("{\"length\":10, \"leaf\":false, \"branches\":[{\"length\":5, \"leaf\":false, \"branches\":[]},{\"length\":5, \"leaf\":false, \"branches\":[]}]}")); //Still no \"leaf\", because no water
        tree.receive("WATER").receive("SUN");
        assertThat(tree.print(), is("{\"length\":11, \"leaf\":false, \"branches\":[{\"length\":5.5, \"leaf\":true, \"branches\":[]},{\"length\":5.5, \"leaf\":true, \"branches\":[]}]}"));
    }

    @Test
    public void movesLeafToNewBranchWhenBranching() {
        tree.receive("WATER");
        tree.receive("SUN");
        assertThat(tree.print(), is("{\"length\":6, \"leaf\":true, \"branches\":[]}"));
        tree.receive("6WATER");
        assertThat(tree.print(), is("{\"length\":12, \"leaf\":true, \"branches\":[]}"));
        tree.receive("FERTILIZER");
        assertThat(tree.print(), is("{\"length\":12, \"leaf\":false, \"branches\":[{\"length\":5, \"leaf\":true, \"branches\":[]}]}"));
    }

    @Test
    public void growsBranchOnYoungestBranchWhichIsLongEnough() {
        tree.receive("5WATER").receive("FERTILIZER");
        assertThat(tree.print(), is("{\"length\":10, \"leaf\":false, \"branches\":[{\"length\":5, \"leaf\":false, \"branches\":[]}]}"));
        tree.receive("4WATER").receive("FERTILIZER");
        assertThat(tree.print(), is("{\"length\":14, \"leaf\":false, \"branches\":[{\"length\":7, \"leaf\":false, \"branches\":[]},{\"length\":5, \"leaf\":false, \"branches\":[]}]}"));
        tree.receive("6WATER");
        assertThat(tree.print(), is("{\"length\":20, \"leaf\":false, \"branches\":[{\"length\":10, \"leaf\":false, \"branches\":[]},{\"length\":8, \"leaf\":false, \"branches\":[]}]}"));
        tree.receive("FERTILIZER");
        assertThat(tree.print(), is("{\"length\":20, \"leaf\":false, \"branches\":[{\"length\":10, \"leaf\":false, \"branches\":[{\"length\":5, \"leaf\":false, \"branches\":[]}]},{\"length\":8, \"leaf\":false, \"branches\":[]}]}"));
    }

    @Test
    public void growsLeavedBranchesOneCmPerHourOfSun() {
        tree.receive("5WATER").receive("FERTILIZER").receive("FERTILIZER");
        tree.receive("WATER").receive("SUN");
        assertThat(tree.print(), is("{\"length\":11, \"leaf\":false, \"branches\":[{\"length\":5.5, \"leaf\":true, \"branches\":[]},{\"length\":5.5, \"leaf\":true, \"branches\":[]}]}"));
        tree.receive("FERTILIZER");
        assertThat(tree.print(), is("{\"length\":11, \"leaf\":false, \"branches\":[{\"length\":5.5, \"leaf\":true, \"branches\":[]},{\"length\":5.5, \"leaf\":true, \"branches\":[]},{\"length\":5, \"leaf\":false, \"branches\":[]}]}"));
        tree.receive("2SUN");
        assertThat(tree.print(), is("{\"length\":11, \"leaf\":false, \"branches\":[{\"length\":7.5, \"leaf\":true, \"branches\":[]},{\"length\":7.5, \"leaf\":true, \"branches\":[]},{\"length\":5, \"leaf\":false, \"branches\":[]}]}"));
        tree.receive("WATER").receive("SUN");
        assertThat(tree.print(), is("{\"length\":12, \"leaf\":false, \"branches\":[{\"length\":9, \"leaf\":true, \"branches\":[]},{\"length\":9, \"leaf\":true, \"branches\":[]},{\"length\":5.5, \"leaf\":true, \"branches\":[]}]}"));
    }
    
}
